package com.example.ud3_ejemplo10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.ud3_ejemplo10.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var binding: ActivityMainBinding

    private var item: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Creamos un adaptador de tipo Arrayadapter usando el string-array creado en el fichero strings.xml
        // y seleccionamos el layout simple_spinner_item.
        // La función 'also' es similar a ´let' salvo que se devuelve el objeto original.
        ArrayAdapter.createFromResource(
            this,
            R.array.numeros,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Especificamos el layout a usar cuando se despliega la lista.
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Aplicamos el adaptador al spinner.
            binding.spinner.adapter = adapter
        }

        // Especificamos la implementación de la interfaz en el spinner
        binding.spinner.onItemSelectedListener = this

        binding.boton.setOnClickListener {
            val intent = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT, item)
            }

            startActivity(intent)
        }

    }

    // Sobreescribimos los dos métodos de la interfaz AdapterView.OnItemSelectedListener
    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // Obtenemos el item seleccionado
        item = parent.getItemAtPosition(pos).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}