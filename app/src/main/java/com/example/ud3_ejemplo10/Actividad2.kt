package com.example.ud3_ejemplo10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo10.databinding.Actividad2Binding

class Actividad2 : AppCompatActivity() {

    private lateinit var binding: Actividad2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad2Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.texto.text = intent.getStringExtra(Intent.EXTRA_TEXT)
    }
}