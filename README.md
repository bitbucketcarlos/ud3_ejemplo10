# Ud3_Ejemplo10
_Ejemplo 10 de la Unidad 3._ Uso de Spinner y envío entre Actividades.

Vamos a crear un _Spinner_ y pasar el dato seleccionado a una Actividad usando _Intets_ explícitos con datos extras.

## _activity_main.xml_ y _actividad2.xml_
En el fichero _activity_main.xml_ definiremos el _Spinner_ y en el layout _actividad2_ símplemente tendremos un _TextView_ donde 
mostraremos el dato seleccionado del _Spinner_.

_activity_main.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <Spinner
        android:id="@+id/spinner"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/boton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/enviar"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/spinner" />


</androidx.constraintlayout.widget.ConstraintLayout>
```
_actividad2.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad2">

    <TextView
        android:id="@+id/texto"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        tools:text="texto" />
</androidx.constraintlayout.widget.ConstraintLayout>
```
## _strings.xml_
Deberemos rellenar los valores del _Spinner_ en el fichero _strings.xml_ utilizando un _string-array_:
```html
<resources>
    <string name="app_name">Ud3_Ejemplo10</string>
    <string name="actividad_2">Actividad2</string>
    <string name="enviar">Enviar</string>
    <string-array name="numeros">
        <item>Uno</item>
        <item>Dos</item>
        <item>Tres</item>
    </string-array>
</resources>
```
## _MainActivity.kt_
En el fichero _MainActivity.kt_ deberemos crear el adaptador para utilizar el _Spinner_ y añadir los datos extra que el _Intent_ pasará
```java
class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var binding: ActivityMainBinding

    private var item: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Creamos un adaptador de tipo Arrayadapter usando el string-array creado en el fichero strings.xml
        // y seleccionamos el layout simple_spinner_item.
        // La función 'also' es similar a ´let' salvo que se devuelve el objeto original.
        ArrayAdapter.createFromResource(
            this,
            R.array.numeros,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Especificamos el layout a usar cuando se despliega la lista.
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Aplicamos el adaptador al spinner.
            binding.spinner.adapter = adapter
        }

        // Especificamos la implementación de la interfaz en el spinner
        binding.spinner.onItemSelectedListener = this

        binding.boton.setOnClickListener {
            val intent = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT, item)
            }

            startActivity(intent)
        }

    }

    // Sobreescribimos los dos métodos de la interfaz AdapterView.OnItemSelectedListener
    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // Obtenemos el item seleccionado
        item = parent.getItemAtPosition(pos).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}

```
## _Actividad2.kt_
Por último, Actividad2 recogerá el dato seleccionado del _Spinner_ y lo mostrará en el _TextView_.
```java
class Actividad2 : AppCompatActivity() {

    private lateinit var binding: Actividad2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad2Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.texto.text = intent.getStringExtra(Intent.EXTRA_TEXT)
    }
}
```